"use strict";

const path = require("path");
const { mkdirSync } = require("fs");

const DbService = require("moleculer-db");

module.exports = function (collection) {
    // Create data folder
    mkdirSync(path.resolve("./data"), { recursive: true });

    return {
        mixins: [DbService],
        adapter: new DbService.MemoryAdapter({ filename: `./data/${collection}.db` })
    };
};
