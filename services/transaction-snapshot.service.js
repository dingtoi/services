"use strict";
const uuidv1 = require('uuid/v1');

const DbService = require('../mixins/db.mixin');

module.exports = {
	name: "transaction-snapshot",

	mixins: [
		DbService("transaction-snapshot"),
	],

	/**
	 * Service settings
	 */
	settings: {
		fields: ["_id", "snapshot", "timestamp"],
	},

	/**
	 * Service dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		add() {
			return this.adapter.insert({
				"_id": uuidv1(),
				"snapshot": {},
				"timestamp": new Date(),
			})
		},
		list() {
			return this.adapter.find({});
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};