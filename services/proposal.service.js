"use strict";
const uuidv1 = require('uuid/v1');

const DbService = require('../mixins/db.mixin');

module.exports = {
	name: "proposal",

	mixins: [
		DbService("proposal"),
	],

	/**
	 * Service settings
	 */
	settings: {
		fields: ["_id", "proposal_id", "event", "payload", "timestamp"],
	},

	/**
	 * Service dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Create new proposal
		 *
		 * @param {String} transaction_id - Transaction id
		 * @param {Number} price - Price
		 */
		add: {
			params: {
				transaction_id: "string",
				price: "number",
			},
			async handler(ctx) {
				const { transaction_id, price } = ctx.params;

				// TODO check transaction_id
				// ctx.call

				const id = uuidv1();
				const payload = {
					transaction_id,
					price,
				};

				const event = await this.adapter.insert({
					_id: id,
					event: "PROPOSAL_CREATED",
					payload,
					timestamp: new Date(),
				});

				// TODO emit event
				// ctx.emit

				return event;
			}
		},
		accept: {
			params: {
				id: "string",
				timestamp: "date",
			},
			async handler(ctx) {
				// TODO validate proposal id
				// TODO check if is proposal owner
				// TODO validate timestamp

				const event = await this.adapter.insert({
					_id: id,
					event: "PROPOSAL_ACCEPTED",
					payload,
					timestamp: new Date(),
				});

				// TODO emmit event
				// TODO include last timestamp for checking
				// ctx.emit

				return event;
			},
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};