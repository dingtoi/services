"use strict";

const DbService = require('../mixins/db.mixin');

module.exports = {
	name: "proposal-snapshot",

	mixins: [
		DbService("proposal-snapshot"),
	],

	/**
	 * Service settings
	 */
	settings: {
		fields: ["_id", "snapshot", "timestamp"],
	},

	/**
	 * Service dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {

	},

	/**
	 * Events
	 */
	events: {
		"proposal.event"(payload) {
			// TODO create new proposal snapshot

			// TODO validate timestamp
			// TODO reconstruct proposal snapshot if timestamp mismatch
		},
	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};